package ru.t1.dkandakov.tm.dto.response.domain;

import lombok.NoArgsConstructor;
import ru.t1.dkandakov.tm.dto.response.AbstractResponse;

@NoArgsConstructor
public final class DataJsonSaveFasterXmlResponse extends AbstractResponse {
}
